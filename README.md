# Visualize NordVPN server locations

A stupid little app. Why don't _they_ do that already?!

See it in action here:
https://dobriai.gitlab.io/nordvpn-mapper/

## Pre-requisites

As things stand of today, Feb 2024, we have some significant bit-rot - i.e. I have not touched/updated the app for an embarrassingly long time!

So, until and if I upgrade it, here goes.

### Yarn

Install `yarn 1.x`, aka _yarn classic_ on your machine. Check with `yarn -v` - should print something like `1.22.19`.

### NodeJS

Need `node 16`, aka _lts/gallium_.

Current `node`s 18 ans 20 fail to build with **ERR_PACKAGE_PATH_NOT_EXPORTED**!

If you are using `nvm-sh`, as you should, there is already a `.nvmrc` in the repo root folder, so just say:
```
nvm use
```
and it should switch to `lts/gallium`, which is the `node 16` LTS.

### MapBox access token (for running locally)

If you want to test the app locally, e.g. via `yarn start` you need to get an access token for MapBox (mapbox.com). Once obtained, the token should be added to the environment, like so:
```
export REACT_APP_TOKEN='blah...'
```
before doing anything.

It is most convenient for the token to have no URL restrictions, so make sure it does not leak into the world, as anyone will be able to use it! Keep it local!

### Updating the server list (optional)

The server list can be re-fetched from this endpoint: `api/server`. I usually prettify it too - e.g. `json_pp` or whatever.

Yes, of course, it should be fetched from the NordVPN site directly, not from a static copy within the app! There was some issue with that, at first. Will get back to it (some day)!

## VANILLA README SECTION Getting: Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `yarn build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
