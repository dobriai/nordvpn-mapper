import React from 'react';
import ReactDOM from 'react-dom';
import mapboxgl from 'mapbox-gl/dist/mapbox-gl-csp';
// eslint-disable-next-line import/no-webpack-loader-syntax
import MapboxWorker from 'worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker';
import './Map.css';

mapboxgl.workerClass = MapboxWorker;
mapboxgl.accessToken = process.env.REACT_APP_TOKEN;

class Map extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      lng: -81.0,
      lat: 43.00,
      zoom: 5.0
    };
    this.mapContainer = React.createRef();
  }
  async componentDidMount() {
    const { lng, lat, zoom } = this.state;
    const map = new mapboxgl.Map({
      container: this.mapContainer.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [lng, lat],
      zoom: zoom
    });

    // Get the data. CORS!
    // const resp = await fetch('https://nordvpn.com/api/server');
    const resp = await fetch('nord-servers.json');
    if (!resp.ok) {
      throw new Error("HTTP error " + resp.status);
    }
    const vpns = await resp.json();

    let seenBefore = new Set();
    const adjustMarkers = () => {
      let seenNow = {};
      const llb = map.getBounds();

      for (const elem of vpns) {
        const { lat, long } = elem.location || 0;
        if (!lat || !long) {
          continue;
        }

        const ll = new mapboxgl.LngLat(long, lat);
        if (!llb.contains(ll)) {
          continue;
        }

        const longLat = [long, lat];
        const key = longLat.toString();

        // Many, many, many servers are in the same place
        if (seenBefore.has(key)) {
          continue;
        }
        (seenNow[key] || (seenNow[key] = { longLat, domains: [] })).domains.push(elem.domain);
      }

      const url = 'https://downloads.nordcdn.com/configs/files/ovpn_legacy/servers/';
      const tcp = '.tcp443.ovpn';
      const udp = '.udp1194.ovpn';
      for (const val of Object.values(seenNow)) {
        const placeholder = document.createElement('div');
        placeholder.classList.add('popup');
        ReactDOM.render(
            val.domains.map(xx => <div><span>{xx} <a href={url+xx+udp}>UDP</a> <a href={url+xx+tcp}>TCP</a></span></div>),
          placeholder
        );

        new mapboxgl.Marker()
          .setLngLat(val.longLat)
          .setPopup(new mapboxgl.Popup().setDOMContent(placeholder))
          .addTo(map);
      }

      for (const key of Object.keys(seenNow)) {
        seenBefore.add(key);
      }
    };

    map.on('move', () => {
      this.setState({
        lng: map.getCenter().lng.toFixed(4),
        lat: map.getCenter().lat.toFixed(4),
        zoom: map.getZoom().toFixed(2)
      });
      adjustMarkers();
    });

    adjustMarkers();
  }

  render() {
    const { lng, lat, zoom } = this.state;
    return (
      <div>
        <div className="sidebar">
          Longitude: {lng} | Latitude: {lat} | Zoom: {zoom}
        </div>
        <div ref={this.mapContainer} className="map-container" />
      </div>
    );
  }
}

export default Map;
